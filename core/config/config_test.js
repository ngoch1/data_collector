var Configuration = require('./configuration').Configuration;
var util = require('util');

new Configuration().load(function (result) {
    console.log(JSON.stringify(result.obj, null, 2));
    console.log(util.inspect("result:" + result));
});