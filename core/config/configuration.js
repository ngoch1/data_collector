var fs = require('fs');
var xml2js = require('xml2js');
var ConfigObject = require("../model/entities");
var PropertyObject = require("../model/properties");

function Configuration() {
    this.load = function (callback) {
        var that = this;
        var parser = new xml2js.Parser();
        fs.readFile(__dirname + '/config.xml', function (err, data) {
            parser.parseString(data, function (err, result) {

                var data = {
                    arr: [],
                    obj: {}
                };

                processEntities(result.entities.entity, data);

                return callback(data);
            });
        });
    };
}

function processEntities(entities, result, parent) {

    for (var i in entities) {

        var entity = entities[i];

        var configObject = new ConfigObject(entity.$.name);

        for (var propIndex in entity.properties) {
            var property = entity.properties[propIndex];
            for (pIndex in property.property) {
                var p = property.property[pIndex].$;

                var po = new PropertyObject(p.key, p.name);

                configObject.setProperty(po);
            }
        }

        if (parent) {
            parent.setChildren(configObject);
        } else {
            result.obj[entity.$.name] = configObject;
            result.arr.push(configObject);
        }

        if (entity.entities) {
            for (i in entity.entities) {
                e = entity.entities[i];
                processEntities(e.entity, result, configObject);
            }
        }
    }
}

module.exports.Configuration = Configuration;