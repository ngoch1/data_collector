var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;

function EntityDAO(db) {

    this.find = function (entityName, callback) {
        db.collection(entityName).find({}).toArray(callback);
    };

    this.save = function (entityName, entity, callback) {
        entity.createDate = new Date();
        if (Array.isArray(entity)) {
            db.collection(entityName).insertMany(entity, callback);
        } else {
            db.collection(entityName).insertOne(entity, callback);
        }

    };

    this.findById = function (entityName, id, callback) {
        db.collection(entityName).findOne({_id: new ObjectID(id)}, callback);
    };

    this.update = function (entityName, entity, id, callback) {
        delete entity._id;

        entity.updateDate = new Date();

        db.collection(entityName).updateOne({_id: new ObjectID(id)}, updateDoc, callback);
    };

    this.delete = function (entityName, id, callback) {
        db.collection(entityName).deleteOne({_id: new ObjectID(id)}, callback);
    }
}

module.exports.EntityDAO = EntityDAO;