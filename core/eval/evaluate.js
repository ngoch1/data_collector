var vm = require('vm');

function Evaluate(config, db) {

    this.eval = function (callback, script) {

        var sandbox = {
            database: db,
            config: config,
            callback: callback
        };

        var vmScript = new vm.Script(script);
        var context = new vm.createContext(sandbox);
        vmScript.runInContext(context);
    }

}
module.exports.Evaluate = Evaluate;