var PropertyObject = function (key, name) {
    this.key = key;
    this.name = name;

    this.getKey = function () {
        return this.key;
    };

    this.getName = function () {
        return this.name;
    }
};

module.exports = PropertyObject;