var EntityObject = function (name) {
    this.name = name;
    this.children = [];
    this.props = [];

    this.setName = function (name) {
        this.name = name;
    };

    this.setChildren = function (child) {
        this.children.push(child);
    };

    this.getChildren = function () {
        return this.children;
    };

    this.setProperty = function (property) {
        this.props.push(property);
    };

    this.getProperties = function () {
        return this.props;
    }
};


module.exports = EntityObject;