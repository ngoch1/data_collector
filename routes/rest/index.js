var Collector = require('./collector');
var Demo = require('./demo');

var API_NAME = "/rest/collector/:entity";

module.exports = function (app, db, config) {

    var collector = new Collector(db, config);

    //Help
    app.get("/rest/help", collector.getHelp);

    //Eval
    app.post("/eval", collector.evalScript);

    //Demo data generator
    var demo = new Demo();
    app.get("/demo/:count", demo.generate);

    /**
     * Entity CRUD
     */
    app.get(API_NAME, collector.load);

    app.post(API_NAME, collector.save);

    app.get(API_NAME + "/:id", collector.findById);

    app.put(API_NAME + "/:id", collector.update);

    app.delete(API_NAME + "/:id", collector.delete);

};