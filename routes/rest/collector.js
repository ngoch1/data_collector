var EntityDAO = require('../../core/dao/entities').EntityDAO;
var Evaluate = require('../../core/eval/evaluate').Evaluate;

function Collector(db, config) {

    var evaluate = new Evaluate(config, db);

    var entityDAO = new EntityDAO(db);

    this.getHelp = function (req, res) {
        res.status(200).json(config.arr);
    };

    this.evalScript = function (req, res) {
        var script = req.body.script;

        if (script) {
            evaluate.eval(function (result) {
                res.status(200).json(result);
            }, script);
        } else {
            res.status(200).json({"status": "empty script"});
        }
    };

    this.load = function (req, res) {
        validateEntity(config, req, res, function (entityName) {
            entityDAO.find(entityName, function (err, docs) {
                if (err) {
                    handleError(res, err.message, "Failed to get contacts.");
                } else {
                    res.status(200).json(docs);
                }
            });
        });
    };

    this.save = function (req, res) {
        validateEntity(config, req, res, function (entityName) {
            entityDAO.save(entityName, req.body, function (err, doc) {
                if (err) {
                    handleError(res, err.message, "Failed to create new contact.");
                } else {
                    res.status(201).json(doc.ops[0]);
                }
            });
        });
    };

    this.findById = function (req, res) {
        validateEntity(config, req, res, function (entityName) {
            entityDAO.findById(entityName, req.params.id, function (err, doc) {
                if (err) {
                    handleError(res, err.message, "Failed to get contact");
                } else {
                    res.status(200).json(doc);
                }
            });
        });
    };

    this.update = function (req, res) {
        validateEntity(config, req, res, function (entityName) {
            entityDAO.update(entityName, req.body, req.params.id, function (err, doc) {
                if (err) {
                    handleError(res, err.message, "Failed to update contact");
                } else {
                    res.status(204).end();
                }
            });
        });
    };

    this.delete = function (req, res) {
        validateEntity(config, req, res, function (entityName) {
            entityDAO.delete(entityName, req.params.id, function (err, result) {
                if (err) {
                    handleError(res, err.message, "Failed to delete contact");
                } else {
                    res.status(204).end();
                }
            });
        });
    };

    function handleError(res, reason, message, code) {
        console.log("ERROR: " + reason);
        res.status(code || 500).json({"error": message});
    }

    function validateEntity(config, req, res, callback) {
        var entityName = req.params.entity;
        if (config.obj[entityName]) {
            callback(entityName);
        } else {
            handleError(res, null, "Entity doesn't exist", 404);
        }
    }
}

module.exports = Collector;