var randomWords = require('random-words');

function Demo() {
    this.generate = function (req, res) {
        var count = req.params.count;
        var doc = [];
        for (var i = 0; i < count; i++) {
            var d = {
                "index": i,
                "name": randomWords()
            };
            doc.push(d);
        }
        res.status(201).json(doc);
    };
}

module.exports = Demo;