var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongodb = require("mongodb");

var index = require('./routes/web/index');
var users = require('./routes/web/users');
var rest = require('./routes/rest');
var Configuration = require('./core/config/configuration').Configuration;

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// Connect to the database before starting the application server.
// var connectionString = "mongodb://dc:fina2demo@ds151137.mlab.com:51137/data_collector";
var connectionString = "mongodb://localhost:27017/dc";
mongodb.MongoClient.connect(process.env.MONGODB_URI || connectionString, function (err, database) {
    if (err) {
        console.log(err);
        process.exit(1);
    }
    console.log("Database connection ready");

    //Load config
    new Configuration().load(function (config) {
        rest(app, database, config);
    });
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
